DROP DATABASE IF EXISTS gestionperson;
CREATE DATABASE gestionperson;
DROP TABLE IF EXISTS gestionperson.person;
CREATE TABLE gestionperson.person (id bigint not null auto_increment, cin varchar(255) not null, nom varchar(255) not null, prenom varchar(255) not null, primary key (id));
