package com.person.api.Mapper;

import com.person.api.Dto.PersonDto;
import com.person.api.Modele.Person;
import fr.xebia.extras.selma.IgnoreMissing;
import fr.xebia.extras.selma.IoC;
import fr.xebia.extras.selma.Mapper;

@Mapper(withIoC = IoC.SPRING, withIgnoreMissing = IgnoreMissing.ALL)
public interface PersonMapper {

    Person personDtoToPerson(PersonDto personDto);
    PersonDto personToPersonDto(Person person);

}
