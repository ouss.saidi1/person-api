package com.person.api.Exception;

public class ModifyingDataException extends  RuntimeException{
    public ModifyingDataException(String message){
        super(message);
    }

}
