package com.person.api.Exception;

public class FetchingDataException extends RuntimeException {

    public FetchingDataException(String message){
        super(message);
    }

}
