package com.person.api.Exception;

public class DataPersistenceException extends RuntimeException {

    public DataPersistenceException(String message){
        super(message);
    }

}
