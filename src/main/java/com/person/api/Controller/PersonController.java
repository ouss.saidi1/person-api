package com.person.api.Controller;

import com.person.api.Dto.PersonDto;
import com.person.api.Service.Interface.IPersonService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/person")
@AllArgsConstructor
public class PersonController {

    IPersonService personService;

    @GetMapping
    public ResponseEntity<List<PersonDto>> getListPerson(){
        return new ResponseEntity<>(personService.getListPerson(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<PersonDto> getPersonById(@PathVariable Long id){
        return new ResponseEntity<>(personService.getPersonById(id),HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<PersonDto> ajouterPerson(@RequestBody @Valid PersonDto personDto){
        return new ResponseEntity<>(personService.ajouter(personDto),HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public void modifierPerson(@RequestBody @Valid PersonDto personDto , @PathVariable Long id){
        personService.modifier(personDto,id);
    }
}
