package com.person.api.Service.Implementation;

import com.person.api.Dao.Interface.IPersonDao;
import com.person.api.Dto.PersonDto;
import com.person.api.Exception.DataPersistenceException;
import com.person.api.Exception.FetchingDataException;
import com.person.api.Exception.ModifyingDataException;
import com.person.api.Mapper.PersonMapper;
import com.person.api.Modele.Person;
import com.person.api.Service.Interface.IPersonService;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import java.util.List;


@Service
@AllArgsConstructor
public class PersonService implements IPersonService {
    private IPersonDao personDao;
    private PersonMapper personMapper;
    @Override
    public PersonDto ajouter( PersonDto personDto) {
        try{
            List<Person> personList = personDao.getListPerson();
            if(personList.stream().anyMatch(p -> p.getCin().equals(personDto.getCin())))
                throw new ModifyingDataException("Cin already used");
            else {
                Person person = personMapper.personDtoToPerson(personDto);
                Person saved =  personDao.ajouter(person);
                return personMapper.personToPersonDto(saved);
            }
        }
        catch (Exception e){
            throw new DataPersistenceException(e.getMessage());
        }

    }

    @Override
    public List<PersonDto> getListPerson() {
        try{
            List<Person> list = personDao.getListPerson();
            if(list.isEmpty()) throw new FetchingDataException("List Vide");
            else return list.stream().map(p -> personMapper.personToPersonDto(p)).toList();
        }catch (Exception e){
            throw new FetchingDataException(e.getMessage());
        }

    }

    @Override
    public PersonDto getPersonById(Long id) {
        try{
            if(id == null) throw new FetchingDataException("Id cannot Be Null");
            else {
                Person person = personDao.getPersonById(id);
                return personMapper.personToPersonDto(person);
            }
        }catch (Exception e){
            throw new FetchingDataException(e.getMessage());
        }
    }

    @Override
    public void modifier(PersonDto personDto, Long id) {
        try{
                Person person = personDao.getPersonById(id);
                //si la modification ne concerne pas le cin
                if(person.getCin().equals(personDto.getCin())){
                    Person toSave = personMapper.personDtoToPerson(personDto);
                    toSave.setId(id);
                    personDao.ajouter(toSave);
                } else{
                    List<Person> listPerson = personDao.getListPerson();
                    if(listPerson.stream().anyMatch(p -> p.getCin().equals(personDto.getCin())))
                        throw new ModifyingDataException("Cin already used");
                    else {
                        Person toSave = personMapper.personDtoToPerson(personDto);
                        toSave.setId(id);
                        personDao.ajouter(person);
                    }
                }
        }catch (Exception e) {
            throw new ModifyingDataException(e.getMessage());
        }

    }

}
