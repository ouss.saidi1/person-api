package com.person.api.Service.Interface;

import com.person.api.Dto.PersonDto;

import java.util.List;

public interface IPersonService {
    PersonDto ajouter(PersonDto personDto);
    List<PersonDto> getListPerson();
    PersonDto getPersonById(Long id);
    void modifier(PersonDto personDto , Long id);
}
