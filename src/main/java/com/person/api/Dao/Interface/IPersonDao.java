package com.person.api.Dao.Interface;


import com.person.api.Modele.Person;

import java.util.List;
import java.util.Optional;

public interface IPersonDao {

    Person ajouter(Person person);
    List<Person> getListPerson();
    Person getPersonById(Long id);
    void delete(Person person);

    void modifier(Person person);


}
