package com.person.api.Dao.Implementaion;


import com.person.api.Dao.Interface.IPersonDao;
import com.person.api.Dao.Repository.PersonRepository;
import com.person.api.Exception.DataPersistenceException;
import com.person.api.Exception.FetchingDataException;
import com.person.api.Exception.ModifyingDataException;
import com.person.api.Modele.Person;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
@AllArgsConstructor
public class PersonDao implements IPersonDao {

    private PersonRepository personRepository;
    @Override
    public Person ajouter(Person person) {
        return personRepository.save(person);
    }

    @Override
    public List<Person> getListPerson() {
        return personRepository.findAll();
    }

    @Override
    public Person getPersonById(Long id) {
        Optional<Person> person = personRepository.findById(id);
        if(person.isEmpty()) throw new FetchingDataException("cannot find person requested");
        else return person.get();
    }

    @Override
    public void delete(Person person) {
            personRepository.delete(person);
    }

    @Override
    public void modifier(Person person) {
        try{
            if(personRepository.existsById(person.getId())){
                personRepository.save(person);
            }else throw new ModifyingDataException("Person Introuvable");
        }catch (Exception e){
            throw new ModifyingDataException("Modifying Failure");
        }
    }

}
