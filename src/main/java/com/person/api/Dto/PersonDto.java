package com.person.api.Dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PersonDto {
    @NotNull(message = "Person nom required.!!")
    @Pattern(regexp = "^[a-zA-Z ]{3,20}$", message = "Invalid nom")
    private String nom;

    @NotNull(message = "Person prenom required.!!")
    @Pattern(regexp = "^[a-zA-Z ]{3,20}$", message = "Invalid prenom")
    private String prenom;

    @NotNull(message = "Person cin required.!!")
    @Pattern(regexp = "^[a-zA-Z]{1,2}[0-9]{3,6}$", message = "Please Enter a valid Cin")
    private String cin;

}
